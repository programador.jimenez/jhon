<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'jhon' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'xG;v8[VwX%}v]~-1qm:Pt5+y7TJ7~r=D9*s2ub*^.5#8vY[@d~KITghlHT-Z}SOI' );
define( 'SECURE_AUTH_KEY',  '/+=4+pj8V(/WcjK-82>DB^IvoHM<^LgRMWHz>p5<RB0#$!C5E1xUd-*8d8& $zgr' );
define( 'LOGGED_IN_KEY',    '-m?$>`m*+B6i^~$%v>K>Lj<nXntPi2y*GovD[Ax|k|:+@[PwoL#@DI0;2k:+%K,w' );
define( 'NONCE_KEY',        ' &RQ4*^lSX?}v}KIz+GyUBo^$b/Jme/~O35tWi0_mD<0bA*.Vz(V[1JxEN,!`Iw}' );
define( 'AUTH_SALT',        '}{1?emO-4IxPgZh3]c<8`KzdWH3=Eq<QyIeugsl;v XKB /X}pVW9#+<$pK947r8' );
define( 'SECURE_AUTH_SALT', '%?yL+=wlVhH|-V3Wz(mA7),i]#Un^0c_?V;;S,95mhil.Y^2^rBcI-wPZxi~v-8C' );
define( 'LOGGED_IN_SALT',   'nm3#aBlH5YDRf)lvX)9_;1h# unlU<ai(vY[>a0_>P?%VjOr`Bxj%2Z4}w`3 uR.' );
define( 'NONCE_SALT',       ')zngECV#S0Ba@RRh8F(vyp_Y#L:- #T-1dF_c2bS(hoa8=@jZEt!>`TC@]$(bnJF' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
